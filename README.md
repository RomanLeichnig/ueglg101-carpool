# UEGLG101 : TD

## Sujet	

En se basant sur les connaissances acquises en cours et en TD, nous réaliserons une API destinée à alimenter une application de covoiturage.

Elle aura un controller contenant deux méthodes : 
- l'une permettant de récupérer la liste des aires de covoiturages des départements passés en paramètres
- l'autre permettant de récupérer la liste des aires de covoiturages à une distance inférieure au nombre de kilomètres passé en paramètres par rapport à votre position courante

Votre position courante est : 49.2032994, 2.5866091

## Règles de gestion

### Aires des départements : 
- Tous les résultats doivent contenir un champ adresse valide (non vide)
- La liste des aires disponibles ne doit contenir que des résultats dont le code postal correspond à la liste des numéros de département passée en paramètre.

Par exemple : si l'utilisateur demande les aires pour le 60, le 02 et le 80, une aire dont le code postal commence par 13 ne pourra pas être contenu dans la liste des résultats

### Aires autour de moi :
- Tous les résultats doivent contenir un champ adresse valide (non vide)
- Tous les résultats doivent se situer à une distance inférieure ou égale à celle passée en paramètre par l'utilisateur

Par exemple: si l'utilsateur demande toutes les aires dans un rayon de 60km, la distance de tous les résultats par rapport à votre position courante doit être inférieure ou égale à 60km.

## Tips

- Vous utiliserez les données importées grâce à la classe DataReader :

    `var areas = DataReader.GetAllAreas();`
- La classe DataReader parse le fichier area.csv embarqué dans le projet pour retourner une liste d'Area. Vous lirez le code du DataReader et ouvrirez le fichier avec excel pour comprendre au mieux la structure de données.
- Le calcul de distance entre deux points GPS s'effectue comme suit : 

    `var currentLocation = new GeoCoordinate(49.2032994, 2.5866091);`
    
    `var otherLocation = new GeoCoordinate(55.334894839, 2.483764);`
    
    `var distanceInKm = currentLocation.GetDistanceTo(otherLocation) / 1000;`

La classe GeoCoordinate est accessible en ajoutant une référence à System.Device (namespace System.Device.Location);

