﻿using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using Cnam.UEGLG101.Journey.Data;

namespace Cnam.UEGLG101.Journey.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var areas = DataReader.GetAllAreas();
            var oise = areas.Where(area => area.PostalCode.StartsWith("60"));

            var currentLocation = new GeoCoordinate(49.2032994, 2.5866091);
            var lessThanFifty = from area in areas
                let location = new GeoCoordinate(area.Latitude, area.Longitude)
                where currentLocation.GetDistanceTo(location) / 1000 <= 50
                select area;
        }
    }
}